<?php

use App\Http\Controllers\LetterController;
use App\Http\Controllers\StandingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('football')->controller(StandingController::class)->group(function () {
    Route::get('/rank', 'show');
    Route::get('/leaguestanding', 'index');
    Route::post('/recordgame', 'store');
});

Route::post('/is-contain-letters', [LetterController::class, 'index']);
