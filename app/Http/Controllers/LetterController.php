<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLetterRequest;
use App\Http\Requests\UpdateLetterRequest;
use App\Models\Letter;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $firstWord = strtolower($request->first_word);
        $secondWord = strtolower($request->second_word);

        return response()->json($this->is_contain($firstWord, $secondWord));
    }

    public function is_contain($first_word, $second_word)
    {
        foreach(str_split($first_word) as $char) {
            $is_contain = Str::contains($second_word, $char);

            if(!$is_contain) return false;
        };

        return true;
    }
}
