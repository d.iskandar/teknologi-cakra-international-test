<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStandingRequest;
use App\Http\Requests\UpdateStandingRequest;
use App\Models\Standing;
use Illuminate\Http\Request;

class StandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Standing::select('clubname', 'points')->orderBy('points', 'DESC')->get();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStandingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStandingRequest $request)
    {
        $clubHome = Standing::firstOrCreate(['clubname' => $request->clubhomename]);
        $clubAway = Standing::firstOrCreate(['clubname' => $request->clubawayname]);

        $score = explode(' : ', $request->score);

        if($score[0] == $score[1]){
            // Seri
            $clubHome->points = $clubHome->points + 1;
            $clubAway->points = $clubAway->points + 1;

        } elseif($score[0] > $score[1]) {
            // Home Win
            $clubHome->points = $clubHome->points + 3;

        } else {
            // Away Win
            $clubAway->points = $clubAway->points + 3;
        }

        $clubHome->save();
        $clubAway->save();

        return response()->json([
            'code' => '202',
            'status' => 'success',
            'message' => 'standing updated!',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Standing  $standing
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $standings = Standing::orderBy('points', 'DESC')->get();

        $rank = $standings->search(function($user) use ($request) {
            return $user->clubname === $request->rank;
        });

        $data = Standing::select('clubname')->where('clubname', $request->rank)->first();
        $data->standing = $rank + 1;

        return response()->json($data);
    }
}
